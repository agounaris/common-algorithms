class Tree:
    def __init__(self, content, left=None, right=None):
        self.content = content
        self.left  = left
        self.right = right

    def __str__(self):
        return str(self.content)
        
        
    def insert_left(self, newNode):
        if self.left == None:
            self.left = Tree(newNode)
        else:
            t = Tree(newNode)
            t.left = self.left
            self.left = t
            
    def insert_right(self, newNode):
        if self.right == None:
            self.right = Tree(newNode)
        else:
            t = Tree(newNode)
            t.right = self.right
            self.right = t