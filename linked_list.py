class Node(object):
    def __init__(self, content=None, next=None):
        self._content = content
        self._next = next
    @property
    def content(self):
        return self.content
    @content.setter
    def content(self, value):
        self._content = value
    def __str__(self):
        return str(self._content)
    def has_next(self):
        return self._next != None
    @property
    def next(self):
        return self._next
    @next.setter
    def next(self, value):
        self._next = value
node_a = Node(1)
node_b = Node(2)
node_a.next = node_b
print(node_a.has_next())
print(node_b.has_next())