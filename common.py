# permutation
def perms(s):        
    if(len(s)==1): return [s]
    result=[]
    for i,v in enumerate(s):
        result += [v+p for p in perms(s[:i]+s[i+1:])]
    return result
    
print(perms('owl'))

def permutate(seq):
    """permutate a sequence and return a list of the permutations"""
    if not seq:
        return [seq]  # is an empty sequence
    else:
        temp = []
        for k in range(len(seq)):
            part = seq[:k] + seq[k+1:]
            for m in permutate(part):
                temp.append(seq[k:k+1] + m)
                #print m, seq[k:k+1], temp  # test
        return temp
        
print(permutate('owl'))

# palindrome
str('radar') == str('radar')[::-1]

# fibonacci
def fibo(n):
    a, b = 0, 1
    for i in range(n):
        yield a
        a, b = b, a + b
        
for i in fibo(5):
    print(i)

